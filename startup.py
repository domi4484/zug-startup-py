
# symlink this file in ~/.local/share/QGIS/QGIS3

import os

import qgis
from qgis.utils import iface
from qgis.core import QgsSettings
from PyQt5.QtCore import Qt, QSize, QTimer
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel

print(f"{__file__}: Start of startup scritp")

def checkEnvironment():
    environment = os.environ.get("ENV", "Environment not set")

    environmentLabel = QLabel(environment)
    environmentLabel.setToolTip("Environment is currently set to '{}'".format(environment))

    pixmap_size = QSize(28, 28)
    pixmap = None
    if environment == "Prod":
        pixmap = QPixmap(pixmap_size)
        pixmap.fill(Qt.green)
    elif environment == "Test":
        pixmap = QPixmap(pixmap_size)
        pixmap.fill(Qt.blue)
    elif environment == "Dev":
        pixmap = QPixmap(pixmap_size)
        pixmap.fill(Qt.red)
        iface.mainWindow().statusBar().showMessage( u"DEV Environment")

    if pixmap != None:
        environmentLabel.setPixmap(pixmap)

    iface.mainWindow().statusBar().insertPermanentWidget(0, environmentLabel)

def activatePlugins():
    print(f"{__file__}: activatePlugins()")
    plugins_to_activate = ["gis-kt-zug_locator",
                           "trackable_project_files"]

    for plugin_to_activate in plugins_to_activate:
        if plugin_to_activate in qgis.utils.active_plugins:
            print(f"{__file__}: Plugin: '{plugin_to_activate}' is already active.")
            continue

        if plugin_to_activate not in qgis.utils.available_plugins:
            print(f"{__file__}: Plugin: '{plugin_to_activate}' is not available. Available plugins: {qgis.utils.available_plugins}")
            continue

    for available_plugin in qgis.utils.available_plugins:
        if not available_plugin in qgis.utils.active_plugins:
            if available_plugin in plugins_to_activate:
                print(f"{__file__}: Load plugin '{available_plugin}'")
                
                # Load and start plugin
                if not qgis.utils.loadPlugin(available_plugin):
                    print(f"{__file__}: Error loading plugin '{available_plugin}'")
                    continue
                if not qgis.utils.startPlugin(available_plugin):
                    print(f"{__file__}: Error starting plugin '{available_plugin}'")
                    continue

                # Make permanent
                settings = QgsSettings()
                settings.setValue( "/PythonPlugins/" + available_plugin, True)
            

checkEnvironment()

# Wait some time before activating plugins to be sure they are already listed
QTimer.singleShot(3*1000, activatePlugins)

print(f"{__file__}: End of startup script")

